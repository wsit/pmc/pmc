package com.workspaceit.pmc.constant;

/**
 * Created by mi_rafi on 1/2/18.
 */
public class UserRole {
    public final static String _SUPER_ADMIN =  "ROLE_superadmin";
    public final static String _ADMIN =  "ROLE_admin";
}

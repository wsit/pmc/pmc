package com.workspaceit.pmc.constant;

/**
 * Created by mi_rafi on 12/28/17.
 */
public class ControllerUriPrefix {
    public final static String API = "/api";
    public final static String ADMIN = "/admin";
    public final static String AUTH_API = "/auth/api";
    public final static String PUBLIC_API = "/public/api";
}

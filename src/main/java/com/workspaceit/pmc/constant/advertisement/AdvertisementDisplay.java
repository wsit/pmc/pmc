package com.workspaceit.pmc.constant.advertisement;

public interface AdvertisementDisplay {
    String getDisplayText();
}

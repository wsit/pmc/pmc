<div class="modal fade in" id="delete-content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeDeleteEntityModal()" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title">Are you sure delete this item?</h4>
            </div>
           <%-- <div class="modal-body">
                <p class="text-center"></p>
            </div>--%>
            <div class="modal-footer">
                <button id="delete-content-no" type="button" class="btn btn-default">No</button>
                <button id="delete-content-yes" type="button" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
</div>